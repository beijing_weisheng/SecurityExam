package com.yx.security.exam.consts;

/**
 * @author YangX
 * @version V1.0
 * @Title Pression
 * @Description
 * @date 2020/4/2 15:53
 **/
public class Pression {

    public final static String READ = "hasAuthority('READ')";
}
