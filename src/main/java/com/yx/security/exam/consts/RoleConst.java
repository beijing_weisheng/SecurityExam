package com.yx.security.exam.consts;

/**
 * @author YangX
 * @version V1.0
 * @Title RoleConst
 * @Description
 * @date 2020/4/2 14:07
 **/
public class RoleConst {

    public static final String ADMIN = "hasAnyRole('ADMIN')";

    public static final String USER = "hasAnyRole('USER')";

    public static final String OTHER = "hasAnyRole('OTHER')";
}
