package com.yx.security.exam.controller;

import com.yx.security.exam.consts.Pression;
import com.yx.security.exam.consts.RoleConst;
import com.yx.security.exam.entity.AuthorityUser;
import com.yx.security.exam.enums.Role;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author YangX
 * @version V1.0
 * @Title AppController
 * @Description
 * @date 2020/3/31 9:18
 **/
@RestController
//@RequestMapping("/app")
public class AppController {

    @GetMapping("/hello")
    public String hello(){
        return "success";
    }

    @PreAuthorize(RoleConst.USER)
    @GetMapping("/user/test")
    public String Test1(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "success_test_user";
    }

    @PreAuthorize(RoleConst.ADMIN)
    @GetMapping("/admin/test")
    public String Test2(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "success_test_admin";
    }

    @PreAuthorize(RoleConst.USER)
    @GetMapping("/other/test")
    public String Test3(){
        return "success_test3";
    }


    @PreAuthorize(Pression.READ)
    @GetMapping("/read")
    public String read(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "success_read_2333333!!!";
    }

    @PreAuthorize(Pression.READ)
    @GetMapping("/write")
    public String write(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return "success_write_2333333!!!";
    }

    @PostMapping("/login/test")
    public String loginTest(){

        Authentication token = SecurityContextHolder.getContext().getAuthentication();
        if(token==null){
            Object principal = token.getPrincipal();
        } else {
            SecurityContextHolder.getContext().setAuthentication(loadUserData());
        }
        return "success_test_login_user!!!";
    }

    private static Authentication loadUserData(){
        List<GrantedAuthority> authorities = new ArrayList<>();
        //authorities.add(new SimpleGrantedAuthority("ROLE_"+ Role.Admin.getRole()));
        authorities.add(new SimpleGrantedAuthority("ROLE_"+Role.User.getRole()));
        authorities.add(new SimpleGrantedAuthority("READ"));

        return new UsernamePasswordAuthenticationToken("user_1", new BCryptPasswordEncoder().encode("111111"), authorities);
    }

}
