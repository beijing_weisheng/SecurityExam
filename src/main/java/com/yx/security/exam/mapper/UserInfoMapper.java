package com.yx.security.exam.mapper;

import com.yx.security.exam.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    UserInfo selectUserInfoByName(@Param("name") String name);

}
