package com.yx.security.exam.mapper;

import com.yx.security.exam.entity.RoleInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
@Repository
public interface RoleInfoMapper extends BaseMapper<RoleInfo> {

    List<RoleInfo> selectRoleByIds(@Param("ids") String ids);

}
