package com.yx.security.exam.config;

import com.yx.security.exam.config.provider.BackDoorAuthenticationProvider;
import com.yx.security.exam.filter.CheckTokenFilter;
import com.yx.security.exam.service.impl.UserDetailServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

/**
 * @author YangX
 * @version V1.0
 * @Title SecurityConfiguration
 * @Description
 * @date 2020/3/6 14:46
 **/
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Resource
    BackDoorAuthenticationProvider backDoorAuthenticationProvider;

    @Resource
    UserDetailServiceImpl userDetailsService;

    @Resource
    SuccessHandler successHandler;

    @Resource
    CheckTokenFilter checkTokenFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //http.authorizeRequests()
        //        .antMatchers("/admin/**").hasAnyRole(Role.User.getRole(), Role.Admin.getRole())
        //        .antMatchers("/user/**").hasRole(Role.User.getRole())
        //        .antMatchers("/other/**").anonymous()//匿名用户
        //        .anyRequest().authenticated()//以上未配置的其他url，需要登录才能访问
        //        .and()
        //        .formLogin().and()
        //        .httpBasic();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests()
                .antMatchers("/login/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                //.loginPage("/login") //自定义登录页
                .loginProcessingUrl("/fc_login")//自定义登录FORM表单请求的路径
                .successHandler(successHandler)//重新定义loginProcessingUrl成功后的Response，避免302
                .and()
                .httpBasic();
        http.addFilterBefore(checkTokenFilter, UsernamePasswordAuthenticationFilter.class);
        http.csrf().disable();
    }

    //@Bean
    //public checkTokenFilter demoFilter(){
    //    return new checkTokenFilter();
    //}

    ////在SecurityConfiguration类中，将BackdoorAuthenticationProvider的实例加入到验证链中
    //@Override
    //protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    //    super.configure(auth);
    //    auth.authenticationProvider(backDoorAuthenticationProvider);
    //}

    //定义密码加加密方式
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //super.configure(auth);TODO 加了会报错，暂时跳过
        auth.userDetailsService(userDetailsService).passwordEncoder(getPasswordEncoder())
        .and().authenticationProvider(backDoorAuthenticationProvider);//添加自定义Authentication校验
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //@Override
    //public void configure(WebSecurity web) {
    //    web.ignoring().antMatchers("/login/test");
    //}

    //@Autowired
    //public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    //
    //    //在内存中添加用户
    //    auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
    //            .withUser("admin").password(new BCryptPasswordEncoder().encode("123456"))
    //            .roles(Role.User.getRole(), Role.Admin.getRole());
    //
    //    //明文方式存储在内存中
    //    //auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance())
    //    //        .withUser("admin").password("777888")
    //    //        .roles(Role.User.getRole(), Role.Admin.getRole());
    //}

}
