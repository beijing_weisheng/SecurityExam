package com.yx.security.exam.config;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.yx.security.exam.util.JsonUtils;
import com.yx.security.exam.vo.RespInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author YangX
 * @version V1.0
 * @Title SuccessHandler
 * @Description
 * @date 2020/4/2 18:04
 **/
@Component("SuccessHandler")
public class SuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    //@Resource
    //private SecurityProperties securityProperties;

    //@Resource
    //private RequestCache requestCache;

    // Authentication  封装认证信息
    // 登录方式不同，Authentication不同
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {

        //if (ObjectUtils.equals(securityProperties.getBrowser().getLoginType(), LoginType.JSON)) {
        //
        //}

        //SavedRequest savedRequest = requestCache.getRequest( request , response );


        logger.info("SuccessHandler login success!");
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST,GET");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        // 把authentication对象转成 json 格式 字符串 通过 response 以application/json;charset=UTF-8 格式写到响应里面去
        //登录成功,返回该用户所具有的权限
//        response.getWriter().write(Objects.requireNonNull(JsonUtils.serialize(
//                new SuccessResp<>(authentication.getAuthorities().stream()
//                        .map(GrantedAuthority::getAuthority)
//                        .collect(Collectors.toList())))));
        response.getWriter().write(Objects.requireNonNull(JsonUtils.serialize(RespInfo.SUCCESS.get())));
    }//TODO 未区分重定向和自然登录
}
