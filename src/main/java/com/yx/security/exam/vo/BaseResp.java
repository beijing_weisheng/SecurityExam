package com.yx.security.exam.vo;

/**
 * @author YangX
 * @version V1.0
 * @Title BaseResp
 * @Description
 * @date 2020/4/2 18:10
 **/
public class BaseResp<T> {

    public BaseResp(T data){
        this.data = data;
    }

    public BaseResp(int code, String msg, T data){
        this.data = data;
        this.code = code;
        this.msg = msg;
    }

    public int code = 0;

    public String msg = null;

    public T data = null;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(T data) {this.data = data; }

}

