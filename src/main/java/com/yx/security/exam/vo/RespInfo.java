package com.yx.security.exam.vo;

/**
 * @author YangX
 * @version V1.0
 * @Title Resp
 * @Description
 * @date 2020/4/2 18:09
 **/
public enum  RespInfo {

    SUCCESS(0, "成功", null);

    private final BaseResp<Object> resp;

    RespInfo(int code, String msg, Object data) {
        this.resp = new BaseResp<>(code, msg, data);
    }

    public BaseResp get() {
        return resp;
    }

    public BaseResp<Object> newInstance() {
        return new BaseResp<>(resp.code, resp.msg, resp.data);
    }
}
