package com.yx.security.exam.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author YangX
 * @version V1.0
 * @Title JSONUtil
 * @Description JSON工具类
 * @date 2019/12/25 11:09
 **/
public class JSONUtil {

    public JSONUtil() {

    }

    public static String toJSONString(Object obj) throws JSONException {
        return getJSONObject(obj).toString();
    }

    public static String toJSONString(Object obj,boolean flag) throws JSONException {

        return getJSONObject(obj,flag).toString();

    }


    public static String getJSONObject(Object value) throws JSONException {

        return JSON.toJSON(value).toString();

    }

    public static String getJSONObject(Object value,boolean flag) throws JSONException {

        return JSON.toJSONString(value, SerializerFeature.DisableCircularReferenceDetect);

    }

    public static String getJSONString(String jsonString, String key)
            throws JSONException {

        JSONObject json = JSONObject.parseObject(jsonString);
        String value = "";
        if(json.containsKey(key)){
            value = json.getString(key);
        }
        return value;

    }
}
