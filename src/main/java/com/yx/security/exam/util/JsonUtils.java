package com.yx.security.exam.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author YangX
 * @version V1.0
 * @Title JsonUtils
 * @Description
 * @date 2020/4/2 18:07
 **/
@Slf4j
public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        // 转换为格式化的json
//        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        // 如果json中有新增的字段并且是实体类类中不存在的，不报错
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
    }


    public static String serialize(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        }catch (JsonProcessingException e){
            log.error("对象转Json字符串出错:", e);
            return null;
        }
    }

    public static <T> T deserialize(String json, Class<T> tClass) {
        try {
            return objectMapper.readValue(json, tClass);
        } catch (IOException e) {
            log.error("Json转对象出错:", e);
            return null;
        }
    }

    public static <T> T deserialize(String json, TypeReference<T> tClass) {
        try {
            return objectMapper.readValue(json, tClass);
        } catch (IOException e) {
            log.error("Json转对象出错:", e);
            return null;
        }
    }
}
