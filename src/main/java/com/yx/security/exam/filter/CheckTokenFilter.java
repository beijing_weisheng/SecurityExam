package com.yx.security.exam.filter;

import com.alibaba.fastjson.JSONObject;
import com.yx.security.exam.util.HtmlUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author YangX
 * @version V1.0
 * @Title checkTokenFilter
 * @Description OncePerRequestFilter 仅执行一次，避免容器和security调用两次
 * @date 2020/4/7 15:55
 **/
@Component
public class CheckTokenFilter extends OncePerRequestFilter {

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        // 其他过滤器已经认证通过了
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            chain.doFilter(request, response);
            return;
        }

        String token = request.getHeader("token");
        if(token!=null && token.startsWith("Test-")){
            //TODO 解析token，将结果重新封装成UsernamePasswordAuthenticationToken，然后SecurityContextHolder.getContext().setAuthentication...

        } else {
            JSONObject result = new JSONObject();
            result.put("code", 401);
            result.put("date", null);
            result.put("msg", "无效的token！");
            HtmlUtil.writerJson(response, result);
        }
        chain.doFilter(request, response);
    }

}
