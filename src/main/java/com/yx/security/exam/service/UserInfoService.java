package com.yx.security.exam.service;

import com.yx.security.exam.entity.AuthorityInfo;
import com.yx.security.exam.entity.RoleInfo;
import com.yx.security.exam.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
public interface UserInfoService extends IService<UserInfo> {

    UserInfo getOneUserInfo(String name);

    List<RoleInfo> getRoleByUser(String roleIds);

    List<AuthorityInfo> getAuthorityByRole(String authIds);

}
