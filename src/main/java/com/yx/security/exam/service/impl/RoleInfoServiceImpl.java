package com.yx.security.exam.service.impl;

import com.yx.security.exam.entity.RoleInfo;
import com.yx.security.exam.mapper.RoleInfoMapper;
import com.yx.security.exam.service.RoleInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
@Service
public class RoleInfoServiceImpl extends ServiceImpl<RoleInfoMapper, RoleInfo> implements RoleInfoService {

}
