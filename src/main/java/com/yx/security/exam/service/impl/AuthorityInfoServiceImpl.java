package com.yx.security.exam.service.impl;

import com.yx.security.exam.entity.AuthorityInfo;
import com.yx.security.exam.mapper.AuthorityInfoMapper;
import com.yx.security.exam.service.AuthorityInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
@Service
public class AuthorityInfoServiceImpl extends ServiceImpl<AuthorityInfoMapper, AuthorityInfo> implements AuthorityInfoService {

}
