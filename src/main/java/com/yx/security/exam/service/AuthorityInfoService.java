package com.yx.security.exam.service;

import com.yx.security.exam.entity.AuthorityInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
public interface AuthorityInfoService extends IService<AuthorityInfo> {

}
