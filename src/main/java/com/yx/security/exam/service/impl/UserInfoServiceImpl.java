package com.yx.security.exam.service.impl;

import com.yx.security.exam.entity.AuthorityInfo;
import com.yx.security.exam.entity.RoleInfo;
import com.yx.security.exam.entity.UserInfo;
import com.yx.security.exam.mapper.AuthorityInfoMapper;
import com.yx.security.exam.mapper.RoleInfoMapper;
import com.yx.security.exam.mapper.UserInfoMapper;
import com.yx.security.exam.service.UserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yx
 * @since 2020-03-31
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private RoleInfoMapper roleInfoMapper;

    @Resource
    private AuthorityInfoMapper authorityInfoMapper;

    @Override
    public UserInfo getOneUserInfo(String name) {
        return userInfoMapper.selectUserInfoByName(name);
    }

    @Override
    public List<RoleInfo> getRoleByUser(String roleIds){
        return roleInfoMapper.selectRoleByIds(roleIds);
    }

    @Override
    public List<AuthorityInfo> getAuthorityByRole(String authIds) {
        return authorityInfoMapper.selectAuthorityByRole(authIds);
    }
}
