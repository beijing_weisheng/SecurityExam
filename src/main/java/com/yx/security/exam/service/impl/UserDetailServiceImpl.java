package com.yx.security.exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yx.security.exam.entity.AuthorityInfo;
import com.yx.security.exam.entity.AuthorityUser;
import com.yx.security.exam.entity.RoleInfo;
import com.yx.security.exam.entity.UserInfo;
import com.yx.security.exam.enums.Role;
import com.yx.security.exam.service.UserInfoService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author YangX
 * @version V1.0
 * @Title UserDetailServiceImpl
 * @Description 重写用户登录逻辑，登录时赋予用户权限
 * @date 2020/4/1 17:04
 **/
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Resource
    private UserInfoService userInfoService;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {

        // TODO 一次查出用户和角色及权限，注意性能问题
        UserInfo user = userInfoService.getOne(new QueryWrapper<UserInfo>().eq("name", name));

        if (StringUtils.isEmpty(user)) {
            //throw new UsernameNotFoundException("UserName: " + name + "not found ...");
            return null;
        }

        List<GrantedAuthority> authorities = new ArrayList<>();

        // hasRole方法会默认替参数添加 “ROLE_”，所以此处需添加“ROLE_”才能准确认证
        if(name.equals("admin")){
            authorities.add(new SimpleGrantedAuthority("ROLE_"+Role.Admin.getRole()));
            authorities.add(new SimpleGrantedAuthority("ROLE_"+Role.User.getRole()));
            authorities.add(new SimpleGrantedAuthority("READ"));
        } else {
            authorities.add(new SimpleGrantedAuthority("ROLE_"+Role.User.getRole()));
        }

        return new AuthorityUser(user.getName(), new BCryptPasswordEncoder().encode(user.getPassword()), "0001", String.valueOf(user.getId()),
                "001", "一号部门", user.getRole(), "角色名XX", authorities);
    }
}
