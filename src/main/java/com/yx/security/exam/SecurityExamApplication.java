package com.yx.security.exam;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@MapperScan(basePackages = {"com.yx.security.exam.mapper"})
@SpringBootApplication
public class SecurityExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityExamApplication.class, args);
    }

}
