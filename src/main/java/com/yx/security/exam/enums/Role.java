package com.yx.security.exam.enums;

public enum Role {

    User("USER"),Admin("ADMIN");

    private final String roleName;

    Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRole() {
        return roleName;
    }
}
