package com.yx.security.exam.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @author YangX
 * @version V1.0
 * @Title AuthorityUser
 * @Description TODO
 * @date 2020/4/1 17:40
 **/
public class AuthorityUser implements UserDetails {

    private String username;

    private String password;

    private String accountName;

    private String userId;

    private String departmentId;

    private String departmentName;

    private String roleId;

    private String roleName;

    private List<GrantedAuthority> authorityList;

    private String token;

    private boolean isEnabled;

    public AuthorityUser(String username, String password, String accountName, String userId, String departmentId,
                         String departmentName, String roleId, String roleName, List<GrantedAuthority> authorityList) {
        this.username = username;
        this.password = password;
        this.accountName = accountName;
        this.userId = userId;
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.authorityList = authorityList;
        this.isEnabled = true;
    }

    public AuthorityUser(String username, boolean isEnabled) {
        this.username = username;
        this.isEnabled = isEnabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorityList;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }
}
